import UserService from "./services/user.service.js";
import EmailService from "./services/email.service.js";
import AuthService from "./services/auth.service.js";

async function startApp() {
    // Start Services
    await UserService.start();
    await EmailService.start();
    await AuthService.start();

    try {
        // Simulate user creation
        const newUser = await UserService.call('user.createUser', {
            username: "Jordan",
            email: "jordan@sample.com"
        });
        console.log("New User Created: ", newUser);
        const users = await UserService.call('user.getUsers');
        console.log("All Users: ", users);

        // Simulate sending email
        const emailResult = await EmailService.call('email.sendEmail', {
            recipient: newUser.email,
            subject: "Welcome to Moleculer!",
            body: "Thank you for signing up and testing Moleculer!"
        });
        console.log("Email Result: ", emailResult);

        // Simulate failed authentication
        const failedAuthResult = await AuthService.call('auth.authUser', {
            username: newUser.username,
            password: "password"
        });
        console.log("Auth Result: ", failedAuthResult);

        // Simulate successful authentication
        const successfulAuthResult = await AuthService.call('auth.authUser', {
            username: "jordan",
            password: "password"
        });
        console.log("Auth Result: ", successfulAuthResult);
    } catch (error) {
        console.error("Error: ", error);
    } finally {
        await UserService.stop();
        await EmailService.stop();
        await AuthService.stop();
    }
};

startApp();
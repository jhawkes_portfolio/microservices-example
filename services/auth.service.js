import { ServiceBroker } from "moleculer";

const broker = new ServiceBroker();

broker.createService({
    name: "auth",
    actions: {
        async authUser(ctx) {
            const { username, password } = ctx.params;

            if (username === "jordan" && password === "password") {
                return {
                    success: true,
                    message: "You are logged in!"
                };
            } else {
                return {
                    success: false,
                    message: "Failed to login!"
                }
            }
        }
    }
});

export default broker;
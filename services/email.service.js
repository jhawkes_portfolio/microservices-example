import { ServiceBroker } from "moleculer";

const broker = new ServiceBroker();

broker.createService({
    name: "email",
    actions: {
        async sendEmail(ctx) {
            const { recipient, subject, body } = ctx.params;
            // Simulated email logic
            console.log(`Sending email to: ${recipient}`);
            console.log(`The Subject is: ${subject}`);
            console.log(`The body is: ${body}`);
            return `Email sent to: ${recipient}`
        }

    }
});

export default broker;
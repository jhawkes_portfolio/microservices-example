# microservices-example

## Name
Sample Microservices Project

## Description
This is a sample microservices project using Moleculer to simulate a sample authentication and email to a new user

## Installation
To run this project use the command `npm index` and view the logs in the terminal

## Author
Jordan Hawkes